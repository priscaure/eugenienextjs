import React, { Component } from 'react'
import Nav from '../Nav/Nav'

import Head from 'next/head'
import Fade from 'react-reveal/Fade';

import NProgress from 'nprogress'
import Router from 'next/router'

Router.onRouteChangeStart = (url) => {
  console.log(`Loading: ${url}`)
  NProgress.start()
}
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

const header = (props) => (
    <div className="headerWrapper">
      <Head>
        <meta charSet="UTF-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="robots" content="noodp" />
        <link rel="canonical" href="https://eugeniemusic.fr" />
        <link rel='stylesheet' type='text/css' href='/nprogress.css' />
        <link rel="shortcut icon" type="image/png" href="/favicon.ico" />
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:type" content="website" />
        <meta name="twitter:card" content="summary"/>
      </Head>
      <Nav/>
        <header key={props.id} style={{ background: `linear-gradient(0deg, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0)), url(${props.featureImg}) center center / cover no-repeat`}} className="header">
          <div className="headerContentWrapper">
            <div className="headerContent">
            <Fade delay={800} ssrFadeout><h1 dangerouslySetInnerHTML={{ __html: props.title }} /></Fade> 
            
            { props.source &&
              <Fade delay={1500} ssrFadeout><span className="source">Photo © <em>{props.source}</em></span></Fade>
            }
            </div>
          </div>
          {props.children}
        </header>
        <style jsx>{`

        .headerWrapper {
          display: flex;
          width: 100%
        }
          
        .header {
          position: relative;
          display: flex;
          width: 100%;
          height: 672px;
        }
          
        .headerContentWrapper {
          display: flex;
          flex: 1;
          justify-content: center;
          align-items: center;
        }
          
        .headerContent {
          width: 860px;
          height: 360px;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-item: center;
        }
          
        .headerContent h1 {
          font-family: 'Falling Sky Bold', Arial, sans-serif;
          font-size: 100px;
          color: #FFFFFF;
          letter-spacing: -7px;
          line-height: 104px;
          text-align: center;
          margin: 10px 0
        }

        .headerContent span.source {
          position: absolute;
          bottom: 30px;
          right: 30px;
          font-size: 12px;
          color: rgba(255,255,255,0.5);
        }
          
        .headerContent span {
          opacity: 0.9;
          font-family: 'MillerText Italic', Arial, sans-serif;
          font-size: 28px;
          color: #FFFFFF;
          line-height: 32px;
          max-width: 760px;
          display: inline-block;
        }

        @media screen and (max-width: 992px){
          .headerContent {
            width: 100%;
            padding: 20px;
          }
        }
        
        @media screen and (max-width: 768px){
          .headerWrapper {
            flex-direction: column;
          }

          .header {
            height: 572px;
          }
        
          .headerContent {
            width: 100%;
            padding: 20px;
          }
        
          .headerContent h1 {
            font-size: 84px;
            line-height: 70px
          }
        }

        @media screen and (max-width: 575px){
          .header {
            height: 272px;
          }

          .headerContent {
            height: 260px;
          }

          .headerContent h1 {
            font-size: 42px;
            line-height: 42px;
            letter-spacing: -3px;
          }
        }
                  
        `}</style>
    </div>
)

export default header;