import React from 'react'
import Link from 'next/link'

const footer = () => (
    
    <footer className="footer">
        <div className="footerContent">
            <span>© Eugénie Music 2018 / Tous droits réservés</span>
            <span><Link href="/page?uri=mentions-legales" as="/page/mentions-legales"><a>Mentions légales</a></Link></span>
        </div>
    </footer>
  
)
export default footer;
