import React from "react"
import Aux from '../../HOC/Aux'

const Section = (props) => (
    <Aux>
        <section className={props.classe} id={props.idName}>
            <h2>{props.sectionTitle}</h2>
            <div className="container">
                {props.children}
            </div>
        </section>
        <style jsx>{`
        section {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            padding: 20px 0;
          }
          
          section h2 {
            font-family: 'Falling Sky Bold', Arial, sans-serif;
            font-size: 62px;
            color: #FFFFFF;
            letter-spacing: -1px;
            line-height: 62px;
            width: 316px;
            margin: 50px auto;
          }
          
          .container {
            display: flex;
            flex-wrap: wrap;
            flex-direction: column;
            justify-content: center;
            align-items: flex-start;
            width: 1220px;
            max-width: 100%;
            padding: 20px;
            margin: 0 auto;
          }

          @media screen and (max-width: 575px){
            section h2 {
                margin: 20px auto;
                font-size: 38px;
                text-align: center;
            }
          }
        `}</style>
    </Aux>
);
export default Section
