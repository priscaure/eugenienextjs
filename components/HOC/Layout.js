import React from 'react'
import { initGA, logPageView } from '../../utils/analytics'
import Aux from './Aux'

export default class Layout extends React.Component {
  componentDidMount () {
    if (!window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()
  }
  render () {
    return (
      <Aux>
        {this.props.children}
      </Aux>
    )
  }
}