import React, { Component } from "react";
import Link from "next/link";
import Aux from "../HOC/Aux";
import fontawesome from "@fortawesome/fontawesome";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/fontawesome-free-solid";

fontawesome.library.add(faBars);

class Nav extends Component {
  burgerToggle() {
    let linksEl = document.querySelector(".narrowLinks");
    if (linksEl.style.display === "block") {
      linksEl.style.display = "none";
    } else {
      linksEl.style.display = "block";
    }
  }

  render() {
    return (
      <Aux>
        <nav className="navBar">
          <div className="brand">
            <Link href="/">
              <a>
                <img
                  src="/logo.svg"
                  width="54"
                  height="67"
                  alt="Eugénie Logo"
                />
              </a>
            </Link>
          </div>
          <div className="navWide">
            <ul className="navigation">
              <li>
                <Link href="/">
                  <a>Home</a>
                </Link>
              </li>
              <li>
                <Link href="/page?uri=a-propos" as="/page/a-propos">
                  <a>À propos</a>
                </Link>
              </li>
              <li>
                <Link href="/page?uri=contact" as="/page/contact">
                  <a>Contact</a>
                </Link>
              </li>
              <li>
                <Link href="/newsletter">
                  <a>Newsletter</a>
                </Link>
              </li>
            </ul>
            <ul className="subNavigation">
              <li>
                <Link href="https://www.facebook.com/EugenieMusicFansite/">
                  <a target="_blank">
                    <svg width="27px" height="27px" viewBox="0 0 27 27">
                      <title>Facebook</title>
                      <g
                        id="nav-fb"
                        stroke="none"
                        strokeWidth="1"
                        fill="none"
                        fillRule="evenodd"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <g
                          id="ic-fb"
                          transform="translate(-106.000000, -412.000000)"
                          stroke="#FFFFFF"
                          strokeWidth="1.25"
                        >
                          <g>
                            <g>
                              <g
                                id="Menu"
                                transform="translate(0.000000, 179.000000)"
                              >
                                <path
                                  d="M130.5,234 L108.5,234 C107.671,234 107,234.671 107,235.5 L107,257.5 C107,258.329 107.671,259 108.5,259 L120,259 L120,251 L117,251 L117,247 L120,247 L120,244 C120,241.239 122.239,239 125,239 L128,239 L128,243 L126,243 C124.896,243 124,243.896 124,245 L124,247 L128,247 L127.5,251 L124,251 L124,259 L130.5,259 C131.329,259 132,258.329 132,257.5 L132,235.5 C132,234.671 131.329,234 130.5,234 Z"
                                  id="Stroke-87"
                                ></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="https://twitter.com/eugeniemusic">
                  <a target="_blank">
                    <svg width="27px" height="22px" viewBox="0 0 27 22">
                      <title>Twitter</title>
                      <g
                        id="nav-twitter"
                        stroke="none"
                        strokeWidth="1"
                        fill="none"
                        fillRule="evenodd"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <g
                          id="ic-twitter"
                          transform="translate(-106.000000, -463.000000)"
                          stroke="#FFFFFF"
                          strokeWidth="1.25"
                        >
                          <g>
                            <g>
                              <g transform="translate(0.000000, 179.000000)">
                                <path
                                  d="M108.630172,286.043103 C110.723276,288.593966 114.47069,291.001724 119.5,291.261207 C119.222414,290.153448 119.19569,287.14569 121.388793,285.896552 C122.369828,285.337069 123.298276,285 124.391379,285 C125.765517,285 126.893966,285.451724 128.093966,286.552586 C129.069828,286.416379 130.894828,285.663793 131.327586,285.368103 C131.04569,286.405172 129.918103,287.737069 129.103448,288.053448 C129.851724,288.051724 131.49569,287.622414 132,287.368966 C131.300862,288.448276 130.080172,289.481034 129.412069,289.909483 C130.185345,296.556897 124.014655,304.818966 115.151724,304.827586 C112.62069,304.830172 109.87069,304.313793 107,302.740517 C109.831897,303.058621 112.866379,302.177586 114.608621,300.653448 C112.411207,300.605172 110.186207,299.183621 109.174138,297.000862 C110.275,297.32931 111.289655,297.496552 111.891379,297.000862 C110.865517,296.890517 107.543103,295.143103 107.543103,291.759483 C108.306897,292.366379 109.406034,292.993103 110.261207,292.826724 C108.862931,292.031034 106.830172,289.00431 108.630172,286.043103 Z"
                                  id="Stroke-89"
                                ></path>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </a>
                </Link>
              </li>
              <li>
                <Link href="https://www.instagram.com/eugeniemusicfr/">
                  <a target="_blank">
                    <svg width="27px" height="27px" viewBox="0 0 27 27">
                      <title>Instagram</title>
                      <g
                        id="nav-insta"
                        stroke="none"
                        strokeWidth="1"
                        fill="none"
                        fillRule="evenodd"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <g
                          id="ic-insta"
                          transform="translate(-106.000000, -508.000000)"
                          stroke="#FFFFFF"
                          strokeWidth="1.25"
                        >
                          <g>
                            <g>
                              <g transform="translate(0.000000, 179.000000)">
                                <g transform="translate(107.000000, 330.000000)">
                                  <path
                                    d="M25,19 C25,22.313 22.313,25 19,25 L6,25 C2.687,25 0,22.313 0,19 L0,6 C0,2.687 2.687,0 6,0 L19,0 C22.313,0 25,2.687 25,6 L25,19 Z"
                                    id="Stroke-38"
                                  ></path>
                                  <path
                                    d="M19,12.5 C19,16.09 16.09,19 12.5,19 C8.91,19 6,16.09 6,12.5 C6,8.91 8.91,6 12.5,6 C16.09,6 19,8.91 19,12.5 Z"
                                    id="Stroke-40"
                                  ></path>
                                  <path
                                    d="M20,3.5 C20,3.776 19.776,4 19.5,4 C19.224,4 19,3.776 19,3.5 C19,3.224 19.224,3 19.5,3 C19.776,3 20,3.224 20,3.5 Z"
                                    id="Stroke-42"
                                  ></path>
                                </g>
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="navNarrow">
            <div className="block-burgerIcon">
              <FontAwesomeIcon
                icon="bars"
                onClick={this.burgerToggle}
                color="white"
                size="lg"
                className="burgerIcon"
              />
            </div>
            <ul className="narrowLinks">
              <li>
                <Link href="/">
                  <a>Home</a>
                </Link>
              </li>
              <li>
                <Link href="/page?uri=a-propos" as="/page/a-propos">
                  <a>À propos</a>
                </Link>
              </li>
              <li>
                <Link href="/page?uri=contact" as="/page/contact">
                  <a>Contact</a>
                </Link>
              </li>
              <li>
                <Link href="/newsletter" as="/page/newsletter">
                  <a>Newsletter</a>
                </Link>
              </li>
              <li>
                <Link href="https://www.facebook.com/EugenieMusicFansite/">
                  <a target="_blank">
                    <img src="/facebook.svg" alt="facebook" />
                  </a>
                </Link>
              </li>
              <li>
                <Link href="https://twitter.com/eugeniemusic">
                  <a target="_blank">
                    <img src="/twitter.svg" alt="twitter" />
                  </a>
                </Link>
              </li>
              <li>
                <Link href="https://www.instagram.com/eugeniemusicfr/">
                  <a target="_blank">
                    <img src="/instagram.svg" alt="instagram" />
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
        <style jsx>{`
          .navBar {
            width: 150px;
            background: #dc4439;
            flex: 1;
          }

          .brand {
            width: 150px;
            height: 150px;
            background-color: #d4216b;
            display: flex;
            justify-content: center;
            align-items: center;
            transition: all 0.3s linear;
          }

          .brand:hover {
            background-color: #26364c;
          }

          .navBar ul {
            padding: 15px;
            width: 120px;
          }

          .navBar ul li {
            text-align: right;
            font-family: "Falling Sky Bold", Arial, sans-serif;
            font-size: 14px;
            line-height: 32px;
            text-transform: uppercase;
            padding: 6px 0;
          }

          .navBar ul li a {
            color: #ffffff;
          }

          .navBar ul li a:hover {
            color: #121212;
          }

          .navWide .subNavigation a svg #ic-fb,
          .navWide .subNavigation a svg #ic-insta,
          .navWide .subNavigation a svg #ic-twitter {
            transition: all 0.2s linear;
          }

          .navWide .subNavigation a:hover svg #ic-fb,
          .navWide .subNavigation a:hover svg #ic-insta,
          .navWide .subNavigation a:hover svg #ic-twitter {
            stroke: #121212;
          }

          .navNarrow {
            display: none;
            transition: cubic-bezier(0.17, 0.04, 0.03, 0.94);
          }

          .navBar .navNarrow .narrowLinks {
            transition: all 0.5s cubic-bezier(0.17, 0.04, 0.03, 0.94);
          }

          @media screen and (max-width: 768px) {
            .navBar {
              display: flex;
              width: 100%;
              height: auto;
            }

            .navBar .brand {
              width: 100px;
              height: 100px;
            }

            .navBar .brand img {
              width: 34px;
            }

            .navWide {
              display: none;
            }

            .navNarrow {
              width: 100%;
              display: flex;
              flex-direction: column;
              transition: cubic-bezier(0.17, 0.04, 0.03, 0.94);
            }

            .navBar .navNarrow .block-burgerIcon {
              display: flex;
              justify-content: flex-end;
              height: 100px;
              align-items: center;
              padding: 0 20px;
            }

            .navBar .navNarrow .block-burgerIcon .burgerIcon {
              display: block;
              cursor: pointer;
            }

            .navBar .navNarrow .narrowLinks {
              display: none;
              width: 100%;
              transition: all 0.5s cubic-bezier(0.17, 0.04, 0.03, 0.94);
            }

            .navBar .navNarrow .narrowLinks li a {
              text-decoration: none;
              display: block;
              padding: 0.5em 0;
              text-align: center;
            }

            @media screen and (max-width: 575px) {
              .navBar .brand {
                width: 80px;
                height: 80px;
              }

              .navBar .brand img {
                width: 24px;
              }

              .navBar .navNarrow .block-burgerIcon {
                height: 80px;
              }

              .navBar .navNarrow .narrowLinks li {
                line-height: 18px;
              }
            }
          }
        `}</style>
      </Aux>
    );
  }
}

export default Nav;
