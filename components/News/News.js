import React, { Component } from "react";
import Aux from "../HOC/Aux";
import Link from "next/link";
import Tilt from "react-tilt";

class News extends Component {
  render() {
    return (
      <Aux>
        <div className="news">
          <Link as={this.props.as} href={this.props.href}>
            <a>
              <div className="newsContent">
                <Tilt className="Tilt" options={{ max: 2, scale: 1.03 }}>
                  <div className="Tilt-inner">
                    <div
                      className="newsImg"
                      style={{
                        background: `linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0)), url(${this.props.imgSrc}) center center / cover no-repeat`
                      }}
                    ></div>
                  </div>
                </Tilt>
                <div className="newsExcerpt">
                  <div className="newsExcerptWrapper">
                    <h3
                      dangerouslySetInnerHTML={{ __html: this.props.newsTitle }}
                    />
                    <div
                      className="newsExcerptContent"
                      dangerouslySetInnerHTML={{
                        __html: this.props.newsExcerpt
                      }}
                    />
                  </div>
                </div>
              </div>
            </a>
          </Link>
        </div>
        <style jsx>{`
          .news {
            max-height: 510px;
            height: 100%;
            margin: 70px auto;
            display: flex;
            justify-content: center;
          }

          .newsContent {
            display: flex;
          }

          .newsImg {
            position: relative;
            width: 532px;
            height: 432px;
            box-shadow: 8px 9px 27px rgba(0, 0, 0, 0.15);
            background-color: #121212;
            border-radius: 8px;
          }

          .newsExcerptWrapper {
            width: 318px;
            height: auto;
          }

          .newsExcerptWrapper h3 {
            font-family: "MillerText Bold Italic", Arial, sans-serif;
            font-size: 32px;
            color: #121212;
            letter-spacing: -1.85px;
            line-height: 36px;
          }

          .newsExcerpt {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 470px;
            height: 442px;
            margin: 30px 0 0 -50px;
            background: linear-gradient(-135deg, #d4216b 0%, #e5897c 100%);
            border-radius: 8px;
            padding: 20px;
          }

          .newsExcerptContent {
            opacity: 0.8;
            font-family: "MillerText Italic", Arial, sans-serif;
            font-size: 20px;
            color: #ffffff;
            letter-spacing: 0;
            line-height: 28px;
          }

          @media screen and (max-width: 768px) {
            .news {
              width: 100%;
              margin: 10px auto;
            }
            .news a {
              width: 100%;
            }
            .newsExcerpt {
              width: 100%;
              height: auto;
              margin: 0;
            }
            .Tilt,
            .Tilt-inner,
            .newsImg {
              display: none;
            }

            @media screen and (max-width: 575px) {
              .newsExcerptWrapper h3 {
                font-size: 24px;
                line-height: 28px;
              }

              .newsExcerptContent {
                font-size: 16px;
                line-height: 22px;
              }
            }
          }
        `}</style>
      </Aux>
    );
  }
}
export default News;
