import React from 'react';
import SubscribeFrom from "react-mailchimp-subscribe";

const formProps = {
  action:
    "https://prizoners.us9.list-manage.com/subscribe/post?u=d66d8c5d1ef09114cf8c27ccb&id=3c7edc9b14",
  styles: {
    sending: {
      fontSize: 14,
      color: "auto"
    },
    success: {
      fontSize: 14,
      color: "green"
    },
    error: {
      fontSize: 14,
      color: "red"
    }
  }
};

const NewsletterForm = () => <SubscribeFrom {...formProps} />;

export default NewsletterForm;