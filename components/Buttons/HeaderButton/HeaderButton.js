import React from "react";
import Link from "next/link";

const headerButton = props => (
  <div>
    <Link href={props.slug} as={props.as}>
      <a className="headerButtonLink">{props.btnText}</a>
    </Link>
    <style jsx>{`
      .headerButtonLink {
        display: block;
        background: #e5897c;
        font-family: "Falling Sky Bold", Arial, sans-serif;
        font-size: 16px;
        text-transform: uppercase;
        color: #ffffff;
        width: 96px;
        height: 96px;
        text-align: center;
        line-height: 96px;
        position: absolute;
        bottom: 0;
        right: 0;
        transition: all 0.3s linear;
      }

      .headerButtonLink:hover {
        background: #d4216b;
      }

      @media screen and (max-width: 768px) {
        .headerButtonLink {
          width: 86px;
          height: 86px;
          line-height: 86px;
          font-size: 16px;
        }
      }

      @media screen and (max-width: 575px) {
        .headerButtonLink {
          width: 66px;
          height: 66px;
          line-height: 66px;
          font-size: 14px;
        }
      }
    `}</style>
  </div>
);
export default headerButton;
