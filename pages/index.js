import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { allPosts } from "../lib/queries/posts";
import withData from "../lib/withData";
import stylesheet from "../styles/style.css";

import Head from "next/head";
import { Fade } from "react-reveal";

import Aux from "../components/HOC/Aux";
import Layout from "../components/HOC/Layout";
import Section from "../components/Layouts/Section/Section";
import Header from "../components/Header/Header";
import HeaderButton from "../components/Buttons/HeaderButton/HeaderButton";
import News from "../components/News/News";
import Footer from "../components/Footer/Footer";

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ""
    };
    this.updateSearch = this.updateSearch.bind(this);
  }

  updateSearch(event) {
    this.setState({ search: event.target.value });
  }

  loadMoreButton(pageInfo, loadMoreEntries) {
    if (pageInfo.hasNextPage) {
      return (
        <Aux>
          <button onClick={loadMoreEntries} className="loadMore">
            Voir plus
          </button>
          ;
          <style jsx>{`
            .loadMore {
              display: block;
              border: none;
              background: #e5897c;
              font-family: "Falling Sky Bold", Arial, sans-serif;
              font-size: 16px;
              text-transform: uppercase;
              color: #ffffff;
              width: 135px;
              height: 56px;
              text-align: center;
              line-height: 56px;
              margin: auto;
              -webkit-transition: all 0.3s linear;
              transition: all 0.3s linear;
              cursor: pointer;
            }

            .loadMore:hover {
              background: #d4216b;
            }

            .loadMore:focus {
              outline: 0;
              box-shadow: none;
            }
          `}</style>
        </Aux>
      );
    }
  }

  render() {
    const { data } = this.props;
    const loadMoreEntries = data.loadMoreEntries;
    console.log(data);

    const loading = data.loading;
    if (loading) {
      return (
        <div>
          <Head>
            <link rel="stylesheet" type="text/css" href="/nprogress.css" />
          </Head>
        </div>
      );
    }

    const postHeader = data.posts.edges[0].node;
    let filteredPosts = [...data.posts.edges].splice(1);

    let searchString = this.state.search.toLowerCase();

    if (searchString.length > 0) {
      // Filter the results.
      filteredPosts = filteredPosts.filter(function(item) {
        return item.node.title.toLowerCase().includes(searchString);
      });
    }

    let listPosts = (
      <Section sectionTitle="Les Eugénews." id="eugenews">
        {filteredPosts.map(item => {
          return (
            <Fade key={item.node.id}>
              <News
                imgSrc={item.node.featuredImage.sourceUrl}
                newsTitle={item.node.title}
                newsExcerpt={item.node.excerpt}
                href={`/post?slug=${item.node.slug}`}
                as={`/post/${item.node.slug}`}
              />
            </Fade>
          );
        })}
        {this.loadMoreButton(data.posts.pageInfo, loadMoreEntries)}
      </Section>
    );

    if (filteredPosts.length === 0) {
      listPosts = (
        <Section sectionTitle="Oups ! Une bouteille à la mer." id="eugenews" />
      );
    }

    return (
      <Layout>
        <Aux>
          <Head>
            <title>Eugénie Music | Fansite Officiel</title>
            <meta
              name="description"
              content="Eugénie Music est le fansite officiel d'Eugénie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eugénie est le nouveau talent de la scène électro-pop française."
            />
            <meta
              property="og:title"
              content="Eugénie, nouveau talent de la scène électro-pop française"
            />
            <meta
              property="og:description"
              content="Eugénie Music est le fansite officiel d'Eugénie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eugénie est le nouveau talent de la scène électro-pop française."
            />
            <meta property="og:url" content="https://eugeniemusic.fr" />
            <meta
              property="og:image"
              content="https://eugeniemusic.fr/og-image.jpg"
            />
            <meta
              name="twitter:title"
              content="Eugénie, nouveau talent de la scène électro-pop française"
            />
            <meta
              name="twitter:description"
              content="Eugénie Music est le fansite officiel d'Eugénie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eugénie est le nouveau talent de la scène électro-pop française."
            />
            <meta
              name="twitter:image:src"
              content="https://eugeniemusic.fr/og-image.jpg"
            />
          </Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <Header
            key={postHeader.id}
            featureImg={postHeader.featuredImage.sourceUrl}
            title={postHeader.title}
          >
            <HeaderButton
              slug={`/post?slug=${postHeader.slug}`}
              as={`/post/${postHeader.slug}`}
              btnText="Lire"
            />
          </Header>
          <Aux>
            <input
              type="text"
              value={this.state.search}
              onChange={this.updateSearch}
              placeholder="Rechercher un article"
              className="searchBox"
            />
            <style jsx>{`
              .searchBox {
                width: 100%;
                height: 56px;
                background: #000000;
                padding: 0 20px;
                border: none;
                color: #f9f9f9;
                text-align: center;
              }
              .searchBox::placeholder {
                /* Chrome, Firefox, Opera, Safari 10.1+ */
                color: rgba(255, 255, 255, 0.2);
                text-align: center;
                opacity: 1; /* Firefox */
              }

              .searchBox:-ms-input-placeholder {
                /* Internet Explorer 10-11 */
                color: rgba(255, 255, 255, 0.2);
              }

              .searchBox::-ms-input-placeholder {
                /* Microsoft Edge */
                color: rgba(255, 255, 255, 0.2);
              }
              .searchBox:focus {
                outline: 0;
              }
            `}</style>
          </Aux>
          {listPosts}
          <Footer />
        </Aux>
      </Layout>
    );
  }
}

export default compose(
  withData,
  graphql(allPosts, {
    props({ data: { loading, posts, fetchMore } }) {
      /**
       * Return the props to connect to the component
       */
      return {
        data: {
          loading,
          posts,
          fetchMore,
          loadMoreEntries: () => {
            console.log(fetchMore);
            return fetchMore({
              query: allPosts,
              variables: {
                cursor: posts.pageInfo.endCursor
              },
              updateQuery: (nextResult, { fetchMoreResult }) => {
                /**
                 * Pluck the new edges out of the query results
                 */
                const newEdges = fetchMoreResult.posts.edges;

                /**
                 * Pluck the new pageInfo out of the query results
                 */
                const pageInfo = fetchMoreResult.posts.pageInfo;

                /**
                 * Return the movies with the new edges merged with the existing ones, and
                 * the new pageInfo replacing the old pageInfo
                 */
                return {
                  posts: {
                    __typename: previousResult.posts.__typename,
                    edges: [...nextResult.posts.edges, ...newEdges],
                    pageInfo
                  }
                };
              }
            });
          }
        }
      };
    }
  })
)(Index);
