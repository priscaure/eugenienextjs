import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { Fade } from "react-reveal";
import Moment from "react-moment";

import Aux from "../components/HOC/Aux";
import Layout from "../components/HOC/Layout";
import Header from "../components/Header/Header";
import Section from "../components/Layouts/Section/Section";
import Footer from "../components/Footer/Footer";

import { singlePost } from "../lib/queries/singlePost";
import withData from "../lib/withData";

import stylesheet from "../styles/style.css";
import Link from "next/link";
import Head from "next/head";

class PostTemplate extends Component {
  static async getInitialProps({ query: { slug } }) {
    await new Promise(resolve => {
      setTimeout(resolve, 600);
    });
    return { slug };
  }

  render() {
    const data = this.props.data;
    const loading = data.loading;

    if (loading) {
      return (
        <div>
          <Head>
            <link rel="stylesheet" type="text/css" href="/nprogress.css" />
          </Head>
        </div>
      );
    }

    const post = this.props.data.postBy;

    return (
      <Layout>
        <Aux>
          <Head>
            <title>{post.title}</title>
            <meta property="og:title" content={`${post.title}`} />
            <meta
              property="og:url"
              content={`https://eugeniemusic.fr/${post.slug}`}
            />
            <meta property="description" content={`${post.excerpt}`} />
            <meta property="og:image" content={post.featuredImage.sourceUrl} />
            <meta name="twitter:title" content={`${post.title}`} />
            <meta name="twitter:description" content={`${post.excerpt}`} />
            <meta
              name="twitter:image:src"
              content={post.featuredImage.sourceUrl}
            />
          </Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <Header
            key={post.id}
            featureImg={post.featuredImage.sourceUrl}
            title={post.title}
            source={post.source}
          />
          <Section classe="singlePost">
            <div className="sectionContentWrapper">
              <Fade delay={700}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: post.content
                  }}
                />
              </Fade>
            </div>
            <div className="singlePostFooter">
              <p>
                Par <a>{post.author.name}</a>, le{" "}
                <span>
                  <Moment locale="fr" format="DD MMMM YYYY">
                    {post.date}
                  </Moment>
                </span>
              </p>
              <p>
                Posté dans :
                <Link
                  as={`/categorie/${post.categories.edges[0].node.slug}`}
                  href={`/categorie?slug=${post.categories.edges[0].node.slug}`}
                >
                  <a>{" " + `${post.categories.edges[0].node.name}`}</a>
                </Link>
              </p>
            </div>
          </Section>
          <Footer />
        </Aux>
      </Layout>
    );
  }
}

export default compose(
  withData,
  graphql(singlePost, {
    options: ({ slug }) => ({ variables: { slug: slug } })
  })
)(PostTemplate);
