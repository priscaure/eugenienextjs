import React from 'react'
import Link from 'next/link'
import Layout from '../components/HOC/Layout'
import Aux from '../components/HOC/Aux'
import Section from '../components/Layouts/Section/Section'
import stylesheet from '../styles/style.css';

import NProgress from 'nprogress'
import Router from 'next/router'

Router.onRouteChangeStart = (url) => {
  console.log(`Loading: ${url}`)
  NProgress.start()
}
Router.onRouteChangeComplete = () => NProgress.done()
Router.onRouteChangeError = () => NProgress.done()

export default class Error extends React.Component {

  render() {
    return (
       <Aux>
           <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
           <div className="Error">
                <div className="ErrorContent">
                    <h1>Oups !</h1>
                    <h3>Il semblerait que ta bouteille à la mer ne soit arrivée à personne… Et si tu filais en vents contraires vers la <Link href="/"><a>page d’accueil</a></Link> ? </h3>
                </div>
           </div>
           <style jsx>{`
                .Error {
                    width: 100%;
                    height: 100vh;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
                .Error h1 {
                    font-family: 'Falling Sky Bold', Arial, sans-serif;
                    font-size: 100px;
                    color: #FFFFFF;
                    letter-spacing: -7px;
                    line-height: 104px;
                    text-align: center;
                    margin: 0 0 36px 0
                }
                .Error h3 {
                    font-family: 'MillerText Bold Italic', Arial, sans-serif;
                    font-size: 38px;
                    color: #FFFFFF;
                    letter-spacing: 0;
                    line-height: 52px;
                    width: 80%;
                    margin: 20px auto 50px auto;
                    text-align: center
                }
                .Error a {
                    color : #d4216b
                }
                .Error a:hover, 
                .Error a:active {
                    color : #E5897C
                }
           `}</style>
       </Aux>
    )
  }
}