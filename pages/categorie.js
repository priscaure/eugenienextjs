import React, { Component } from "react";
import { graphql, compose } from "react-apollo";

import Aux from "../components/HOC/Aux";
import Layout from "../components/HOC/Layout";
import Section from "../components/Layouts/Section/Section";
import News from "../components/News/News";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";

import { CategoriesBy } from "../lib/queries/categoriesBy";
import withData from "../lib/withData";
import stylesheet from "../styles/style.css";

import Link from "next/link";
import Head from "next/head";

class CategoryTemplate extends Component {
  static async getInitialProps({ query: { slug } }) {
    return { slug };
  }

  render() {
    const { data } = this.props;
    const loading = data.loading;

    if (loading) {
      return (
        <div>
          <Head>
            <link
              rel="stylesheet"
              type="text/css"
              href="/nprogress.css"
            />
          </Head>
        </div>
      );
    }

    const categoryName = data.categories.edges[0].node.name;

    return (
      <Layout>
        <Aux>
          <Head>
            <title dangerouslySetInnerHTML={{ __html: categoryName }} />
            <meta charSet="utf-8" />
            <meta
              name="viewport"
              content="initial-scale=1.0, width=device-width"
            />
          </Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
          <Header
            key={data.categories.edges[0].node.id}
            featureImg={
              data.categories.edges[0].node.posts.edges[0].node.featuredImage
                .sourceUrl
            }
            title={`Categorie : ${categoryName}`}
          />
          <Section sectionTitle="Les Posts" classe="categorie" id="categorie">
            <div className="categoryWrapper">
              {data.categories.edges[0].node.posts.edges.map((item, i) => {
                return (
                  <Link
                    href={`/post?slug=${item.node.slug}`}
                    as={`/post/${item.node.slug}`}
                  >
                    <a key={item.node.id}>
                      <div
                        className="newsExcerpt"
                        style={{
                          background: `linear-gradient(-135deg, rgba(212, 33, 107, 1) 0%, rgba(229, 137, 124, 0.8) 100%), url(${item.node.featuredImage.sourceUrl}) center center / cover no-repeat`
                        }}
                      >
                        <div className="newsExcerptWrapper">
                          <h3
                            dangerouslySetInnerHTML={{
                              __html: item.node.title
                            }}
                          />
                        </div>
                      </div>
                    </a>
                  </Link>
                );
              })}
            </div>
          </Section>
          <Footer />
        </Aux>
        <style jsx>{`
          header h1 {
            text-align: center;
          }
          .categoryWrapper {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
          }

          .categoryWrapper a {
            max-width: 270px;
            margin: 20px;
            width: 100%;
          }

          .newsExcerpt {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 270px;
            height: 242px;
            background: linear-gradient(-135deg, #d4216b 0%, #e5897c 100%);
            border-radius: 8px;
            padding: 20px;
          }
          .newsExcerptWrapper {
            max-width: 230px;
            width: 100%;
            height: auto;
          }

          .newsExcerptWrapper h3 {
            font-family: "MillerText Bold Italic", Arial, sans-serif;
            font-size: 24px;
            color: #121212;
            letter-spacing: -1.85px;
            line-height: 28px;
            text-align: center;
          }
        `}</style>
      </Layout>
    );
  }
}

export default compose(
  withData,
  graphql(CategoriesBy, {
    options: ({ slug }) => ({ variables: { slug: slug } })
  })
)(CategoryTemplate);
