import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { Fade } from "react-reveal";

import Aux from "../components/HOC/Aux";
import Layout from "../components/HOC/Layout";
import Header from "../components/Header/Header";
import Section from "../components/Layouts/Section/Section";
import Footer from "../components/Footer/Footer";

import { singlePage } from "../lib/queries/page";
import withData from "../lib/withData";

import stylesheet from "../styles/style.css";
import Link from "next/link";
import Head from "next/head";

class PageTemplate extends Component {
  static async getInitialProps({ query: { uri } }) {
    await new Promise(resolve => {
      setTimeout(resolve, 600);
    });
    return { uri };
  }

  render() {
    const data = this.props.data;
    const loading = data.loading;

    if (loading) {
      console.log(data);
      return (
        <div>
          <Head>
            <link rel="stylesheet" type="text/css" href="/nprogress.css" />
          </Head>
        </div>
      );
    }

    const page = this.props.data.pageBy;

    return (
      <Layout>
        <Aux>
          <Head>
            <title>{page.title}</title>
          </Head>
          <style dangerouslySetInnerHTML={{ __html: stylesheet }} />

          <Header key={page.id} title={page.title} />

          <Section classe="singlePost">
            <div className="sectionContentWrapper">
              <Fade delay={500}>
                <div
                  dangerouslySetInnerHTML={{
                    __html: page.content
                  }}
                />
              </Fade>
            </div>
          </Section>
          <Footer />
        </Aux>
      </Layout>
    );
  }
}

export default compose(
  withData,
  graphql(singlePage, {
    options: ({ uri }) => ({ variables: { uri: uri } })
  })
)(PageTemplate);
