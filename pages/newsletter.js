import React, { Component } from "react"
//import { graphql, compose } from 'react-apollo'
import { Fade } from 'react-reveal';
import SubscribeFrom from "react-mailchimp-subscribe";

import Aux from '../components/HOC/Aux'
import Layout from '../components/HOC/Layout'
import Header from '../components/Header/Header'
import Section from '../components/Layouts/Section/Section'
import Footer from "../components/Footer/Footer";

//import { singlePage } from "../lib/queries/page"
//import withData from '../lib/withData'

import stylesheet from '../styles/style.css'
import Link from 'next/link'
import Head from 'next/head'

class Newsletter extends Component {
    
    render() {
        const formProps = {
            action: "https://eugeniemusic.us17.list-manage.com/subscribe/post?u=dd0dd276e6340d449c5579366&id=1f1fab7c4d",
            messages: {
                inputPlaceholder: "Email",
                btnLabel: "S'abonner aux Eugénews",
                sending: "Envoi en cours...",
                success: "Ta bouteille à la mer a bien été reçue ! Un email t'as été envoyé pour confirmer ton abonnement.",
                error: "Oops, impossible d'enregistrer cette adresse."
            },
            styles: {
              sending: {
                fontSize: 16,
                color: "auto"
              },
              success: {
                fontSize: 16,
                color: "green"
              },
              error: {
                fontSize: 16,
                color: "red"
              },
            }
          };
          
          const Form = () => <SubscribeFrom {...formProps} />;
        return (
            <Layout>
                <Aux>
                    <Head><title>Newsletter</title></Head>
                    <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
                    <Header title="Newsletter"/>
                    <Section classe="singlePost">
                        <div className="sectionContentWrapper">
                            <Fade delay={500}>
                                <h3>Si tu en as marre des bouteilles à la mer qui n'arrivent à personne. 
                                    Inscris-toi aux Eugénews, elles arriveront dans ta boite mail.
                                </h3>
                                <div className="singlePostContent">
                                    <Form />          
                                </div>
                            </Fade>
                        </div>
                    </Section>
                    <Footer/>
                </Aux>
            </Layout>
        )
    }
}

export default Newsletter
