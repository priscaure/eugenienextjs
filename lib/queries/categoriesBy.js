import gql from 'graphql-tag'

export const CategoriesBy = gql`
  query Category {
    categories {
        edges {
            node {
                id
                slug
                name
                posts {
                    edges {
                        node {
                            id
                            title
                            slug
                            featuredImage{
                                sourceUrl
                            }
                        }
                    }
                }
            }
        }
    }
}
`