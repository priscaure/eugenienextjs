import gql from "graphql-tag";

export const singlePost = gql`
  query singlePost($slug: String!) {
    postBy(slug: $slug) {
      slug
      id
      title
      excerpt
      content
      featuredImage {
        sourceUrl
      }
      date
      author {
        name
      }
      categories {
        edges {
          node {
            id
            slug
            name
          }
        }
      }
    }
  }
`;
