import gql from "graphql-tag";

export const singlePage = gql`
  query singlePage($uri: String) {
    pageBy(uri: $uri) {
      uri
      slug
      id
      title
      content
    }
  }
`;
