import gql from 'graphql-tag'

export const allPages = gql`
query allPages {
    pages {
        edges {
            node{
                id
                title
                uri
                slug
            }
        }
    }

}
`