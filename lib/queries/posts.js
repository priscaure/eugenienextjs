import gql from "graphql-tag";

export const allPosts = gql`
  query allPosts($cursor: String) {
    posts(
      first: 10
      where: { orderby: { field: DATE, order: DESC } }
      before: $cursor
    ) {
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          id
          title
          excerpt
          slug
          featuredImage {
            sourceUrl
          }
        }
      }
    }
  }
`;
