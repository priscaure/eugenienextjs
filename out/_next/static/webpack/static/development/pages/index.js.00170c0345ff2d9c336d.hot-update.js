webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/toConsumableArray */ "./node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-apollo */ "./node_modules/react-apollo/react-apollo.browser.umd.js");
/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_apollo__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _lib_queries_posts__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../lib/queries/posts */ "./lib/queries/posts.js");
/* harmony import */ var _lib_withData__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../lib/withData */ "./lib/withData.js");
/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../styles/style.css */ "./styles/style.css");
/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_styles_style_css__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var react_reveal__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! react-reveal */ "./node_modules/react-reveal/index.js");
/* harmony import */ var react_reveal__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(react_reveal__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _components_HOC_Aux__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../components/HOC/Aux */ "./components/HOC/Aux.js");
/* harmony import */ var _components_HOC_Layout__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../components/HOC/Layout */ "./components/HOC/Layout.js");
/* harmony import */ var _components_Layouts_Section_Section__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../components/Layouts/Section/Section */ "./components/Layouts/Section/Section.js");
/* harmony import */ var _components_Header_Header__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../components/Header/Header */ "./components/Header/Header.js");
/* harmony import */ var _components_Buttons_HeaderButton_HeaderButton__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../components/Buttons/HeaderButton/HeaderButton */ "./components/Buttons/HeaderButton/HeaderButton.js");
/* harmony import */ var _components_News_News__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../components/News/News */ "./components/News/News.js");
/* harmony import */ var _components_Footer_Footer__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../components/Footer/Footer */ "./components/Footer/Footer.js");










var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;















var Index =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(Index, _Component);

  function Index(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_1__["default"])(this, Index);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__["default"])(Index).call(this, props));
    _this.state = {
      search: ""
    };
    _this.updateSearch = _this.updateSearch.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__["default"])(_this));
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_2__["default"])(Index, [{
    key: "updateSearch",
    value: function updateSearch(event) {
      this.setState({
        search: event.target.value
      });
    }
  }, {
    key: "loadMoreButton",
    value: function loadMoreButton(pageInfo, loadMoreEntries) {
      if (pageInfo.hasNextPage) {
        return __jsx(_components_HOC_Aux__WEBPACK_IMPORTED_MODULE_15__["default"], null, __jsx("button", {
          onClick: loadMoreEntries,
          className: "jsx-4256669768" + " " + "loadMore"
        }, "Voir plus"), ";", __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
          id: "4256669768"
        }, ".loadMore.jsx-4256669768{display:block;border:none;background:#e5897c;font-family:\"Falling Sky Bold\",Arial,sans-serif;font-size:16px;text-transform:uppercase;color:#ffffff;width:135px;height:56px;text-align:center;line-height:56px;margin:auto;-webkit-transition:all 0.3s linear;-webkit-transition:all 0.3s linear;transition:all 0.3s linear;cursor:pointer;}.loadMore.jsx-4256669768:hover{background:#d4216b;}.loadMore.jsx-4256669768:focus{outline:0;box-shadow:none;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hdXJlL0RvY3VtZW50cy9Qcm9qZXRzL0V1Z2VuaWUvcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBc0NzQixBQUVpQixBQWlCSyxBQUdULFVBQ00sSUFwQkosS0FnQlEsT0FmRCxBQW1CRixtQkFsQmlDLGdEQUNuQyxlQUNVLHlCQUNYLGNBQ0YsWUFDQSxZQUNNLGtCQUNELGlCQUNMLFlBQ3VCLG1DQUNSLDhEQUNaLGVBQUMiLCJmaWxlIjoiL1VzZXJzL2F1cmUvRG9jdW1lbnRzL1Byb2pldHMvRXVnZW5pZS9wYWdlcy9pbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IGdyYXBocWwsIGNvbXBvc2UgfSBmcm9tIFwicmVhY3QtYXBvbGxvXCI7XG5pbXBvcnQgeyBhbGxQb3N0cyB9IGZyb20gXCIuLi9saWIvcXVlcmllcy9wb3N0c1wiO1xuaW1wb3J0IHdpdGhEYXRhIGZyb20gXCIuLi9saWIvd2l0aERhdGFcIjtcbmltcG9ydCBzdHlsZXNoZWV0IGZyb20gXCIuLi9zdHlsZXMvc3R5bGUuY3NzXCI7XG5cbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcbmltcG9ydCB7IEZhZGUgfSBmcm9tIFwicmVhY3QtcmV2ZWFsXCI7XG5cbmltcG9ydCBBdXggZnJvbSBcIi4uL2NvbXBvbmVudHMvSE9DL0F1eFwiO1xuaW1wb3J0IExheW91dCBmcm9tIFwiLi4vY29tcG9uZW50cy9IT0MvTGF5b3V0XCI7XG5pbXBvcnQgU2VjdGlvbiBmcm9tIFwiLi4vY29tcG9uZW50cy9MYXlvdXRzL1NlY3Rpb24vU2VjdGlvblwiO1xuaW1wb3J0IEhlYWRlciBmcm9tIFwiLi4vY29tcG9uZW50cy9IZWFkZXIvSGVhZGVyXCI7XG5pbXBvcnQgSGVhZGVyQnV0dG9uIGZyb20gXCIuLi9jb21wb25lbnRzL0J1dHRvbnMvSGVhZGVyQnV0dG9uL0hlYWRlckJ1dHRvblwiO1xuaW1wb3J0IE5ld3MgZnJvbSBcIi4uL2NvbXBvbmVudHMvTmV3cy9OZXdzXCI7XG5pbXBvcnQgRm9vdGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0Zvb3Rlci9Gb290ZXJcIjtcblxuY2xhc3MgSW5kZXggZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgc2VhcmNoOiBcIlwiXG4gICAgfTtcbiAgICB0aGlzLnVwZGF0ZVNlYXJjaCA9IHRoaXMudXBkYXRlU2VhcmNoLmJpbmQodGhpcyk7XG4gIH1cblxuICB1cGRhdGVTZWFyY2goZXZlbnQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgc2VhcmNoOiBldmVudC50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBsb2FkTW9yZUJ1dHRvbihwYWdlSW5mbywgbG9hZE1vcmVFbnRyaWVzKSB7XG4gICAgaWYgKHBhZ2VJbmZvLmhhc05leHRQYWdlKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8QXV4PlxuICAgICAgICAgIDxidXR0b24gb25DbGljaz17bG9hZE1vcmVFbnRyaWVzfSBjbGFzc05hbWU9XCJsb2FkTW9yZVwiPlxuICAgICAgICAgICAgVm9pciBwbHVzXG4gICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgO1xuICAgICAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgICAgIC5sb2FkTW9yZSB7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlNTg5N2M7XG4gICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIkZhbGxpbmcgU2t5IEJvbGRcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgICAgICAgICAgY29sb3I6ICNmZmZmZmY7XG4gICAgICAgICAgICAgIHdpZHRoOiAxMzVweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA1NnB4O1xuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiA1NnB4O1xuICAgICAgICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuM3MgbGluZWFyO1xuICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBsaW5lYXI7XG4gICAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmxvYWRNb3JlOmhvdmVyIHtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2Q0MjE2YjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmxvYWRNb3JlOmZvY3VzIHtcbiAgICAgICAgICAgICAgb3V0bGluZTogMDtcbiAgICAgICAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBgfTwvc3R5bGU+XG4gICAgICAgIDwvQXV4PlxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBkYXRhIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGxvYWRNb3JlRW50cmllcyA9IGRhdGEubG9hZE1vcmVFbnRyaWVzO1xuICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgY29uc3QgbG9hZGluZyA9IGRhdGEubG9hZGluZztcbiAgICBpZiAobG9hZGluZykge1xuICAgICAgcmV0dXJuIChcbiAgICAgICAgPGRpdj5cbiAgICAgICAgICA8SGVhZD5cbiAgICAgICAgICAgIDxsaW5rIHJlbD1cInN0eWxlc2hlZXRcIiB0eXBlPVwidGV4dC9jc3NcIiBocmVmPVwiL25wcm9ncmVzcy5jc3NcIiAvPlxuICAgICAgICAgIDwvSGVhZD5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cblxuICAgIGNvbnN0IHBvc3RIZWFkZXIgPSBkYXRhLnBvc3RzLmVkZ2VzWzBdLm5vZGU7XG4gICAgbGV0IGZpbHRlcmVkUG9zdHMgPSBbLi4uZGF0YS5wb3N0cy5lZGdlc10uc3BsaWNlKDEpO1xuXG4gICAgbGV0IHNlYXJjaFN0cmluZyA9IHRoaXMuc3RhdGUuc2VhcmNoLnRvTG93ZXJDYXNlKCk7XG5cbiAgICBpZiAoc2VhcmNoU3RyaW5nLmxlbmd0aCA+IDApIHtcbiAgICAgIC8vIEZpbHRlciB0aGUgcmVzdWx0cy5cbiAgICAgIGZpbHRlcmVkUG9zdHMgPSBmaWx0ZXJlZFBvc3RzLmZpbHRlcihmdW5jdGlvbihpdGVtKSB7XG4gICAgICAgIHJldHVybiBpdGVtLm5vZGUudGl0bGUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hTdHJpbmcpO1xuICAgICAgfSk7XG4gICAgfVxuXG4gICAgbGV0IGxpc3RQb3N0cyA9IChcbiAgICAgIDxTZWN0aW9uIHNlY3Rpb25UaXRsZT1cIkxlcyBFdWfDqW5ld3MuXCIgaWQ9XCJldWdlbmV3c1wiPlxuICAgICAgICB7ZmlsdGVyZWRQb3N0cy5tYXAoaXRlbSA9PiB7XG4gICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIDxGYWRlIGtleT17aXRlbS5ub2RlLmlkfT5cbiAgICAgICAgICAgICAgPE5ld3NcbiAgICAgICAgICAgICAgICBpbWdTcmM9e2l0ZW0ubm9kZS5mZWF0dXJlZEltYWdlLnNvdXJjZVVybH1cbiAgICAgICAgICAgICAgICBuZXdzVGl0bGU9e2l0ZW0ubm9kZS50aXRsZX1cbiAgICAgICAgICAgICAgICBuZXdzRXhjZXJwdD17aXRlbS5ub2RlLmV4Y2VycHR9XG4gICAgICAgICAgICAgICAgaHJlZj17YC9wb3N0P3NsdWc9JHtpdGVtLm5vZGUuc2x1Z31gfVxuICAgICAgICAgICAgICAgIGFzPXtgL3Bvc3QvJHtpdGVtLm5vZGUuc2x1Z31gfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9GYWRlPlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgICB7dGhpcy5sb2FkTW9yZUJ1dHRvbihkYXRhLnBvc3RzLnBhZ2VJbmZvLCBsb2FkTW9yZUVudHJpZXMpfVxuICAgICAgPC9TZWN0aW9uPlxuICAgICk7XG5cbiAgICBpZiAoZmlsdGVyZWRQb3N0cy5sZW5ndGggPT09IDApIHtcbiAgICAgIGxpc3RQb3N0cyA9IChcbiAgICAgICAgPFNlY3Rpb24gc2VjdGlvblRpdGxlPVwiT3VwcyAhIFVuZSBib3V0ZWlsbGUgw6AgbGEgbWVyLlwiIGlkPVwiZXVnZW5ld3NcIiAvPlxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gKFxuICAgICAgPExheW91dD5cbiAgICAgICAgPEF1eD5cbiAgICAgICAgICA8SGVhZD5cbiAgICAgICAgICAgIDx0aXRsZT5FdWfDqW5pZSBNdXNpYyB8IEZhbnNpdGUgT2ZmaWNpZWw8L3RpdGxlPlxuICAgICAgICAgICAgPG1ldGFcbiAgICAgICAgICAgICAgbmFtZT1cImRlc2NyaXB0aW9uXCJcbiAgICAgICAgICAgICAgY29udGVudD1cIkV1Z8OpbmllIE11c2ljIGVzdCBsZSBmYW5zaXRlIG9mZmljaWVsIGQnRXVnw6luaWUuIEF2ZWMgc2VzIHR1YmVzICdQdWlzIERhbnNlJyBldCAnU3VyIGxhIG1lcicsIEV1Z8OpbmllIGVzdCBsZSBub3V2ZWF1IHRhbGVudCBkZSBsYSBzY8OobmUgw6lsZWN0cm8tcG9wIGZyYW7Dp2Fpc2UuXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8bWV0YVxuICAgICAgICAgICAgICBwcm9wZXJ0eT1cIm9nOnRpdGxlXCJcbiAgICAgICAgICAgICAgY29udGVudD1cIkV1Z8OpbmllLCBub3V2ZWF1IHRhbGVudCBkZSBsYSBzY8OobmUgw6lsZWN0cm8tcG9wIGZyYW7Dp2Fpc2VcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxtZXRhXG4gICAgICAgICAgICAgIHByb3BlcnR5PVwib2c6ZGVzY3JpcHRpb25cIlxuICAgICAgICAgICAgICBjb250ZW50PVwiRXVnw6luaWUgTXVzaWMgZXN0IGxlIGZhbnNpdGUgb2ZmaWNpZWwgZCdFdWfDqW5pZS4gQXZlYyBzZXMgdHViZXMgJ1B1aXMgRGFuc2UnIGV0ICdTdXIgbGEgbWVyJywgRXVnw6luaWUgZXN0IGxlIG5vdXZlYXUgdGFsZW50IGRlIGxhIHNjw6huZSDDqWxlY3Ryby1wb3AgZnJhbsOnYWlzZS5cIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxtZXRhIHByb3BlcnR5PVwib2c6dXJsXCIgY29udGVudD1cImh0dHBzOi8vZXVnZW5pZW11c2ljLmZyXCIgLz5cbiAgICAgICAgICAgIDxtZXRhXG4gICAgICAgICAgICAgIHByb3BlcnR5PVwib2c6aW1hZ2VcIlxuICAgICAgICAgICAgICBjb250ZW50PVwiaHR0cHM6Ly9ldWdlbmllbXVzaWMuZnIvb2ctaW1hZ2UuanBnXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8bWV0YVxuICAgICAgICAgICAgICBuYW1lPVwidHdpdHRlcjp0aXRsZVwiXG4gICAgICAgICAgICAgIGNvbnRlbnQ9XCJFdWfDqW5pZSwgbm91dmVhdSB0YWxlbnQgZGUgbGEgc2PDqG5lIMOpbGVjdHJvLXBvcCBmcmFuw6dhaXNlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8bWV0YVxuICAgICAgICAgICAgICBuYW1lPVwidHdpdHRlcjpkZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgIGNvbnRlbnQ9XCJFdWfDqW5pZSBNdXNpYyBlc3QgbGUgZmFuc2l0ZSBvZmZpY2llbCBkJ0V1Z8OpbmllLiBBdmVjIHNlcyB0dWJlcyAnUHVpcyBEYW5zZScgZXQgJ1N1ciBsYSBtZXInLCBFdWfDqW5pZSBlc3QgbGUgbm91dmVhdSB0YWxlbnQgZGUgbGEgc2PDqG5lIMOpbGVjdHJvLXBvcCBmcmFuw6dhaXNlLlwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPG1ldGFcbiAgICAgICAgICAgICAgbmFtZT1cInR3aXR0ZXI6aW1hZ2U6c3JjXCJcbiAgICAgICAgICAgICAgY29udGVudD1cImh0dHBzOi8vZXVnZW5pZW11c2ljLmZyL29nLWltYWdlLmpwZ1wiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvSGVhZD5cbiAgICAgICAgICA8c3R5bGUgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e3sgX19odG1sOiBzdHlsZXNoZWV0IH19IC8+XG4gICAgICAgICAgPEhlYWRlclxuICAgICAgICAgICAga2V5PXtwb3N0SGVhZGVyLmlkfVxuICAgICAgICAgICAgZmVhdHVyZUltZz17cG9zdEhlYWRlci5mZWF0dXJlZEltYWdlLnNvdXJjZVVybH1cbiAgICAgICAgICAgIHRpdGxlPXtwb3N0SGVhZGVyLnRpdGxlfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxIZWFkZXJCdXR0b25cbiAgICAgICAgICAgICAgc2x1Zz17YC9wb3N0P3NsdWc9JHtwb3N0SGVhZGVyLnNsdWd9YH1cbiAgICAgICAgICAgICAgYXM9e2AvcG9zdC8ke3Bvc3RIZWFkZXIuc2x1Z31gfVxuICAgICAgICAgICAgICBidG5UZXh0PVwiTGlyZVwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvSGVhZGVyPlxuICAgICAgICAgIDxBdXg+XG4gICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5zZWFyY2h9XG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLnVwZGF0ZVNlYXJjaH1cbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJSZWNoZXJjaGVyIHVuIGFydGljbGVcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzZWFyY2hCb3hcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgICAgICAgLnNlYXJjaEJveCB7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA1NnB4O1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMwMDAwMDA7XG4gICAgICAgICAgICAgICAgcGFkZGluZzogMCAyMHB4O1xuICAgICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgICAgICBjb2xvcjogI2Y5ZjlmOTtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgLnNlYXJjaEJveDo6cGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgICAgIC8qIENocm9tZSwgRmlyZWZveCwgT3BlcmEsIFNhZmFyaSAxMC4xKyAqL1xuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDE7IC8qIEZpcmVmb3ggKi9cbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIC5zZWFyY2hCb3g6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgICAgICAgICAvKiBJbnRlcm5ldCBFeHBsb3JlciAxMC0xMSAqL1xuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMik7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAuc2VhcmNoQm94OjotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgICAgIC8qIE1pY3Jvc29mdCBFZGdlICovXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuc2VhcmNoQm94OmZvY3VzIHtcbiAgICAgICAgICAgICAgICBvdXRsaW5lOiAwO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBgfTwvc3R5bGU+XG4gICAgICAgICAgPC9BdXg+XG4gICAgICAgICAge2xpc3RQb3N0c31cbiAgICAgICAgICA8Rm9vdGVyIC8+XG4gICAgICAgIDwvQXV4PlxuICAgICAgPC9MYXlvdXQ+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBjb21wb3NlKFxuICB3aXRoRGF0YSxcbiAgZ3JhcGhxbChhbGxQb3N0cywge1xuICAgIHByb3BzKHsgZGF0YTogeyBsb2FkaW5nLCBwb3N0cywgZmV0Y2hNb3JlIH0gfSkge1xuICAgICAgLyoqXG4gICAgICAgKiBSZXR1cm4gdGhlIHByb3BzIHRvIGNvbm5lY3QgdG8gdGhlIGNvbXBvbmVudFxuICAgICAgICovXG4gICAgICByZXR1cm4ge1xuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgbG9hZGluZyxcbiAgICAgICAgICBwb3N0cyxcbiAgICAgICAgICBmZXRjaE1vcmUsXG4gICAgICAgICAgbG9hZE1vcmVFbnRyaWVzOiAoKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gZmV0Y2hNb3JlKHtcbiAgICAgICAgICAgICAgcXVlcnk6IGFsbFBvc3RzLFxuICAgICAgICAgICAgICB2YXJpYWJsZXM6IHtcbiAgICAgICAgICAgICAgICBjdXJzb3I6IHBvc3RzLnBhZ2VJbmZvLmVuZEN1cnNvclxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB1cGRhdGVRdWVyeTogKG5leHRSZXN1bHQsIHsgZmV0Y2hNb3JlUmVzdWx0IH0pID0+IHtcbiAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgKiBQbHVjayB0aGUgbmV3IGVkZ2VzIG91dCBvZiB0aGUgcXVlcnkgcmVzdWx0c1xuICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgIGNvbnN0IG5ld0VkZ2VzID0gZmV0Y2hNb3JlUmVzdWx0LnBvc3RzLmVkZ2VzO1xuXG4gICAgICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgICAgICogUGx1Y2sgdGhlIG5ldyBwYWdlSW5mbyBvdXQgb2YgdGhlIHF1ZXJ5IHJlc3VsdHNcbiAgICAgICAgICAgICAgICAgKi9cbiAgICAgICAgICAgICAgICBjb25zdCBwYWdlSW5mbyA9IGZldGNoTW9yZVJlc3VsdC5wb3N0cy5wYWdlSW5mbztcblxuICAgICAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICAgICAqIFJldHVybiB0aGUgbW92aWVzIHdpdGggdGhlIG5ldyBlZGdlcyBtZXJnZWQgd2l0aCB0aGUgZXhpc3Rpbmcgb25lcywgYW5kXG4gICAgICAgICAgICAgICAgICogdGhlIG5ldyBwYWdlSW5mbyByZXBsYWNpbmcgdGhlIG9sZCBwYWdlSW5mb1xuICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICBwb3N0czoge1xuICAgICAgICAgICAgICAgICAgICBlZGdlczogWy4uLm5leHRSZXN1bHQucG9zdHMuZWRnZXMsIC4uLm5ld0VkZ2VzXSxcbiAgICAgICAgICAgICAgICAgICAgcGFnZUluZm9cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfVxuICB9KVxuKShJbmRleCk7XG4iXX0= */\n/*@ sourceURL=/Users/aure/Documents/Projets/Eugenie/pages/index.js */"));
      }
    }
  }, {
    key: "render",
    value: function render() {
      var data = this.props.data;
      var loadMoreEntries = data.loadMoreEntries;
      console.log(data);
      var loading = data.loading;

      if (loading) {
        return __jsx("div", null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_13___default.a, null, __jsx("link", {
          rel: "stylesheet",
          type: "text/css",
          href: "/nprogress.css"
        })));
      }

      var postHeader = data.posts.edges[0].node;

      var filteredPosts = Object(_babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(data.posts.edges).splice(1);

      var searchString = this.state.search.toLowerCase();

      if (searchString.length > 0) {
        // Filter the results.
        filteredPosts = filteredPosts.filter(function (item) {
          return item.node.title.toLowerCase().includes(searchString);
        });
      }

      var listPosts = __jsx(_components_Layouts_Section_Section__WEBPACK_IMPORTED_MODULE_17__["default"], {
        sectionTitle: "Les Eug\xE9news.",
        id: "eugenews"
      }, filteredPosts.map(function (item) {
        return __jsx(react_reveal__WEBPACK_IMPORTED_MODULE_14__["Fade"], {
          key: item.node.id
        }, __jsx(_components_News_News__WEBPACK_IMPORTED_MODULE_20__["default"], {
          imgSrc: item.node.featuredImage.sourceUrl,
          newsTitle: item.node.title,
          newsExcerpt: item.node.excerpt,
          href: "/post?slug=".concat(item.node.slug),
          as: "/post/".concat(item.node.slug)
        }));
      }), this.loadMoreButton(data.posts.pageInfo, loadMoreEntries));

      if (filteredPosts.length === 0) {
        listPosts = __jsx(_components_Layouts_Section_Section__WEBPACK_IMPORTED_MODULE_17__["default"], {
          sectionTitle: "Oups ! Une bouteille \xE0 la mer.",
          id: "eugenews"
        });
      }

      return __jsx(_components_HOC_Layout__WEBPACK_IMPORTED_MODULE_16__["default"], null, __jsx(_components_HOC_Aux__WEBPACK_IMPORTED_MODULE_15__["default"], null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_13___default.a, null, __jsx("title", null, "Eug\xE9nie Music | Fansite Officiel"), __jsx("meta", {
        name: "description",
        content: "Eug\xE9nie Music est le fansite officiel d'Eug\xE9nie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eug\xE9nie est le nouveau talent de la sc\xE8ne \xE9lectro-pop fran\xE7aise."
      }), __jsx("meta", {
        property: "og:title",
        content: "Eug\xE9nie, nouveau talent de la sc\xE8ne \xE9lectro-pop fran\xE7aise"
      }), __jsx("meta", {
        property: "og:description",
        content: "Eug\xE9nie Music est le fansite officiel d'Eug\xE9nie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eug\xE9nie est le nouveau talent de la sc\xE8ne \xE9lectro-pop fran\xE7aise."
      }), __jsx("meta", {
        property: "og:url",
        content: "https://eugeniemusic.fr"
      }), __jsx("meta", {
        property: "og:image",
        content: "https://eugeniemusic.fr/og-image.jpg"
      }), __jsx("meta", {
        name: "twitter:title",
        content: "Eug\xE9nie, nouveau talent de la sc\xE8ne \xE9lectro-pop fran\xE7aise"
      }), __jsx("meta", {
        name: "twitter:description",
        content: "Eug\xE9nie Music est le fansite officiel d'Eug\xE9nie. Avec ses tubes 'Puis Danse' et 'Sur la mer', Eug\xE9nie est le nouveau talent de la sc\xE8ne \xE9lectro-pop fran\xE7aise."
      }), __jsx("meta", {
        name: "twitter:image:src",
        content: "https://eugeniemusic.fr/og-image.jpg"
      })), __jsx("style", {
        dangerouslySetInnerHTML: {
          __html: _styles_style_css__WEBPACK_IMPORTED_MODULE_12___default.a
        }
      }), __jsx(_components_Header_Header__WEBPACK_IMPORTED_MODULE_18__["default"], {
        key: postHeader.id,
        featureImg: postHeader.featuredImage.sourceUrl,
        title: postHeader.title
      }, __jsx(_components_Buttons_HeaderButton_HeaderButton__WEBPACK_IMPORTED_MODULE_19__["default"], {
        slug: "/post?slug=".concat(postHeader.slug),
        as: "/post/".concat(postHeader.slug),
        btnText: "Lire"
      })), __jsx(_components_HOC_Aux__WEBPACK_IMPORTED_MODULE_15__["default"], null, __jsx("input", {
        type: "text",
        value: this.state.search,
        onChange: this.updateSearch,
        placeholder: "Rechercher un article",
        className: "jsx-2546289256" + " " + "searchBox"
      }), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_7___default.a, {
        id: "2546289256"
      }, ".searchBox.jsx-2546289256{width:100%;height:56px;background:#000000;padding:0 20px;border:none;color:#f9f9f9;text-align:center;}.searchBox.jsx-2546289256::-webkit-input-placeholder{color:rgba(255,255,255,0.2);text-align:center;opacity:1;}.searchBox.jsx-2546289256::-moz-placeholder{color:rgba(255,255,255,0.2);text-align:center;opacity:1;}.searchBox.jsx-2546289256:-ms-input-placeholder{color:rgba(255,255,255,0.2);text-align:center;opacity:1;}.searchBox.jsx-2546289256::placeholder{color:rgba(255,255,255,0.2);text-align:center;opacity:1;}.searchBox.jsx-2546289256:-ms-input-placeholder{color:rgba(255,255,255,0.2);}.searchBox.jsx-2546289256::-ms-input-placeholder{color:rgba(255,255,255,0.2);}.searchBox.jsx-2546289256:focus{outline:0;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hdXJlL0RvY3VtZW50cy9Qcm9qZXRzL0V1Z2VuaWUvcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBbUx3QixBQUVjLEFBVW9CLEFBT0EsQUFJQSxBQUdyQixVQUFDLENBdkJDLFlBQ08sS0FTRCxBQU1jLEFBSUEsY0FsQmpCLElBU0wsVUFDSSxDQVRGLFlBQ0UsY0FDSSxrQkFBQyIsImZpbGUiOiIvVXNlcnMvYXVyZS9Eb2N1bWVudHMvUHJvamV0cy9FdWdlbmllL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgZ3JhcGhxbCwgY29tcG9zZSB9IGZyb20gXCJyZWFjdC1hcG9sbG9cIjtcbmltcG9ydCB7IGFsbFBvc3RzIH0gZnJvbSBcIi4uL2xpYi9xdWVyaWVzL3Bvc3RzXCI7XG5pbXBvcnQgd2l0aERhdGEgZnJvbSBcIi4uL2xpYi93aXRoRGF0YVwiO1xuaW1wb3J0IHN0eWxlc2hlZXQgZnJvbSBcIi4uL3N0eWxlcy9zdHlsZS5jc3NcIjtcblxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xuaW1wb3J0IHsgRmFkZSB9IGZyb20gXCJyZWFjdC1yZXZlYWxcIjtcblxuaW1wb3J0IEF1eCBmcm9tIFwiLi4vY29tcG9uZW50cy9IT0MvQXV4XCI7XG5pbXBvcnQgTGF5b3V0IGZyb20gXCIuLi9jb21wb25lbnRzL0hPQy9MYXlvdXRcIjtcbmltcG9ydCBTZWN0aW9uIGZyb20gXCIuLi9jb21wb25lbnRzL0xheW91dHMvU2VjdGlvbi9TZWN0aW9uXCI7XG5pbXBvcnQgSGVhZGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0hlYWRlci9IZWFkZXJcIjtcbmltcG9ydCBIZWFkZXJCdXR0b24gZnJvbSBcIi4uL2NvbXBvbmVudHMvQnV0dG9ucy9IZWFkZXJCdXR0b24vSGVhZGVyQnV0dG9uXCI7XG5pbXBvcnQgTmV3cyBmcm9tIFwiLi4vY29tcG9uZW50cy9OZXdzL05ld3NcIjtcbmltcG9ydCBGb290ZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvRm9vdGVyL0Zvb3RlclwiO1xuXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBzZWFyY2g6IFwiXCJcbiAgICB9O1xuICAgIHRoaXMudXBkYXRlU2VhcmNoID0gdGhpcy51cGRhdGVTZWFyY2guYmluZCh0aGlzKTtcbiAgfVxuXG4gIHVwZGF0ZVNlYXJjaChldmVudCkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBzZWFyY2g6IGV2ZW50LnRhcmdldC52YWx1ZSB9KTtcbiAgfVxuXG4gIGxvYWRNb3JlQnV0dG9uKHBhZ2VJbmZvLCBsb2FkTW9yZUVudHJpZXMpIHtcbiAgICBpZiAocGFnZUluZm8uaGFzTmV4dFBhZ2UpIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxBdXg+XG4gICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXtsb2FkTW9yZUVudHJpZXN9IGNsYXNzTmFtZT1cImxvYWRNb3JlXCI+XG4gICAgICAgICAgICBWb2lyIHBsdXNcbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICA7XG4gICAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgICAgLmxvYWRNb3JlIHtcbiAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2U1ODk3YztcbiAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IFwiRmFsbGluZyBTa3kgQm9sZFwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICAgICAgICAgICAgd2lkdGg6IDEzNXB4O1xuICAgICAgICAgICAgICBoZWlnaHQ6IDU2cHg7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDU2cHg7XG4gICAgICAgICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBsaW5lYXI7XG4gICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGxpbmVhcjtcbiAgICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAubG9hZE1vcmU6aG92ZXIge1xuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZDQyMTZiO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAubG9hZE1vcmU6Zm9jdXMge1xuICAgICAgICAgICAgICBvdXRsaW5lOiAwO1xuICAgICAgICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIGB9PC9zdHlsZT5cbiAgICAgICAgPC9BdXg+XG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGRhdGEgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgbG9hZE1vcmVFbnRyaWVzID0gZGF0YS5sb2FkTW9yZUVudHJpZXM7XG4gICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICBjb25zdCBsb2FkaW5nID0gZGF0YS5sb2FkaW5nO1xuICAgIGlmIChsb2FkaW5nKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxIZWFkPlxuICAgICAgICAgICAgPGxpbmsgcmVsPVwic3R5bGVzaGVldFwiIHR5cGU9XCJ0ZXh0L2Nzc1wiIGhyZWY9XCIvbnByb2dyZXNzLmNzc1wiIC8+XG4gICAgICAgICAgPC9IZWFkPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICk7XG4gICAgfVxuXG4gICAgY29uc3QgcG9zdEhlYWRlciA9IGRhdGEucG9zdHMuZWRnZXNbMF0ubm9kZTtcbiAgICBsZXQgZmlsdGVyZWRQb3N0cyA9IFsuLi5kYXRhLnBvc3RzLmVkZ2VzXS5zcGxpY2UoMSk7XG5cbiAgICBsZXQgc2VhcmNoU3RyaW5nID0gdGhpcy5zdGF0ZS5zZWFyY2gudG9Mb3dlckNhc2UoKTtcblxuICAgIGlmIChzZWFyY2hTdHJpbmcubGVuZ3RoID4gMCkge1xuICAgICAgLy8gRmlsdGVyIHRoZSByZXN1bHRzLlxuICAgICAgZmlsdGVyZWRQb3N0cyA9IGZpbHRlcmVkUG9zdHMuZmlsdGVyKGZ1bmN0aW9uKGl0ZW0pIHtcbiAgICAgICAgcmV0dXJuIGl0ZW0ubm9kZS50aXRsZS50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKHNlYXJjaFN0cmluZyk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBsZXQgbGlzdFBvc3RzID0gKFxuICAgICAgPFNlY3Rpb24gc2VjdGlvblRpdGxlPVwiTGVzIEV1Z8OpbmV3cy5cIiBpZD1cImV1Z2VuZXdzXCI+XG4gICAgICAgIHtmaWx0ZXJlZFBvc3RzLm1hcChpdGVtID0+IHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgPEZhZGUga2V5PXtpdGVtLm5vZGUuaWR9PlxuICAgICAgICAgICAgICA8TmV3c1xuICAgICAgICAgICAgICAgIGltZ1NyYz17aXRlbS5ub2RlLmZlYXR1cmVkSW1hZ2Uuc291cmNlVXJsfVxuICAgICAgICAgICAgICAgIG5ld3NUaXRsZT17aXRlbS5ub2RlLnRpdGxlfVxuICAgICAgICAgICAgICAgIG5ld3NFeGNlcnB0PXtpdGVtLm5vZGUuZXhjZXJwdH1cbiAgICAgICAgICAgICAgICBocmVmPXtgL3Bvc3Q/c2x1Zz0ke2l0ZW0ubm9kZS5zbHVnfWB9XG4gICAgICAgICAgICAgICAgYXM9e2AvcG9zdC8ke2l0ZW0ubm9kZS5zbHVnfWB9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0ZhZGU+XG4gICAgICAgICAgKTtcbiAgICAgICAgfSl9XG4gICAgICAgIHt0aGlzLmxvYWRNb3JlQnV0dG9uKGRhdGEucG9zdHMucGFnZUluZm8sIGxvYWRNb3JlRW50cmllcyl9XG4gICAgICA8L1NlY3Rpb24+XG4gICAgKTtcblxuICAgIGlmIChmaWx0ZXJlZFBvc3RzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgbGlzdFBvc3RzID0gKFxuICAgICAgICA8U2VjdGlvbiBzZWN0aW9uVGl0bGU9XCJPdXBzICEgVW5lIGJvdXRlaWxsZSDDoCBsYSBtZXIuXCIgaWQ9XCJldWdlbmV3c1wiIC8+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICA8TGF5b3V0PlxuICAgICAgICA8QXV4PlxuICAgICAgICAgIDxIZWFkPlxuICAgICAgICAgICAgPHRpdGxlPkV1Z8OpbmllIE11c2ljIHwgRmFuc2l0ZSBPZmZpY2llbDwvdGl0bGU+XG4gICAgICAgICAgICA8bWV0YVxuICAgICAgICAgICAgICBuYW1lPVwiZGVzY3JpcHRpb25cIlxuICAgICAgICAgICAgICBjb250ZW50PVwiRXVnw6luaWUgTXVzaWMgZXN0IGxlIGZhbnNpdGUgb2ZmaWNpZWwgZCdFdWfDqW5pZS4gQXZlYyBzZXMgdHViZXMgJ1B1aXMgRGFuc2UnIGV0ICdTdXIgbGEgbWVyJywgRXVnw6luaWUgZXN0IGxlIG5vdXZlYXUgdGFsZW50IGRlIGxhIHNjw6huZSDDqWxlY3Ryby1wb3AgZnJhbsOnYWlzZS5cIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxtZXRhXG4gICAgICAgICAgICAgIHByb3BlcnR5PVwib2c6dGl0bGVcIlxuICAgICAgICAgICAgICBjb250ZW50PVwiRXVnw6luaWUsIG5vdXZlYXUgdGFsZW50IGRlIGxhIHNjw6huZSDDqWxlY3Ryby1wb3AgZnJhbsOnYWlzZVwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPG1ldGFcbiAgICAgICAgICAgICAgcHJvcGVydHk9XCJvZzpkZXNjcmlwdGlvblwiXG4gICAgICAgICAgICAgIGNvbnRlbnQ9XCJFdWfDqW5pZSBNdXNpYyBlc3QgbGUgZmFuc2l0ZSBvZmZpY2llbCBkJ0V1Z8OpbmllLiBBdmVjIHNlcyB0dWJlcyAnUHVpcyBEYW5zZScgZXQgJ1N1ciBsYSBtZXInLCBFdWfDqW5pZSBlc3QgbGUgbm91dmVhdSB0YWxlbnQgZGUgbGEgc2PDqG5lIMOpbGVjdHJvLXBvcCBmcmFuw6dhaXNlLlwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPG1ldGEgcHJvcGVydHk9XCJvZzp1cmxcIiBjb250ZW50PVwiaHR0cHM6Ly9ldWdlbmllbXVzaWMuZnJcIiAvPlxuICAgICAgICAgICAgPG1ldGFcbiAgICAgICAgICAgICAgcHJvcGVydHk9XCJvZzppbWFnZVwiXG4gICAgICAgICAgICAgIGNvbnRlbnQ9XCJodHRwczovL2V1Z2VuaWVtdXNpYy5mci9vZy1pbWFnZS5qcGdcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxtZXRhXG4gICAgICAgICAgICAgIG5hbWU9XCJ0d2l0dGVyOnRpdGxlXCJcbiAgICAgICAgICAgICAgY29udGVudD1cIkV1Z8OpbmllLCBub3V2ZWF1IHRhbGVudCBkZSBsYSBzY8OobmUgw6lsZWN0cm8tcG9wIGZyYW7Dp2Fpc2VcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxtZXRhXG4gICAgICAgICAgICAgIG5hbWU9XCJ0d2l0dGVyOmRlc2NyaXB0aW9uXCJcbiAgICAgICAgICAgICAgY29udGVudD1cIkV1Z8OpbmllIE11c2ljIGVzdCBsZSBmYW5zaXRlIG9mZmljaWVsIGQnRXVnw6luaWUuIEF2ZWMgc2VzIHR1YmVzICdQdWlzIERhbnNlJyBldCAnU3VyIGxhIG1lcicsIEV1Z8OpbmllIGVzdCBsZSBub3V2ZWF1IHRhbGVudCBkZSBsYSBzY8OobmUgw6lsZWN0cm8tcG9wIGZyYW7Dp2Fpc2UuXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8bWV0YVxuICAgICAgICAgICAgICBuYW1lPVwidHdpdHRlcjppbWFnZTpzcmNcIlxuICAgICAgICAgICAgICBjb250ZW50PVwiaHR0cHM6Ly9ldWdlbmllbXVzaWMuZnIvb2ctaW1hZ2UuanBnXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9IZWFkPlxuICAgICAgICAgIDxzdHlsZSBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IHN0eWxlc2hlZXQgfX0gLz5cbiAgICAgICAgICA8SGVhZGVyXG4gICAgICAgICAgICBrZXk9e3Bvc3RIZWFkZXIuaWR9XG4gICAgICAgICAgICBmZWF0dXJlSW1nPXtwb3N0SGVhZGVyLmZlYXR1cmVkSW1hZ2Uuc291cmNlVXJsfVxuICAgICAgICAgICAgdGl0bGU9e3Bvc3RIZWFkZXIudGl0bGV9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPEhlYWRlckJ1dHRvblxuICAgICAgICAgICAgICBzbHVnPXtgL3Bvc3Q/c2x1Zz0ke3Bvc3RIZWFkZXIuc2x1Z31gfVxuICAgICAgICAgICAgICBhcz17YC9wb3N0LyR7cG9zdEhlYWRlci5zbHVnfWB9XG4gICAgICAgICAgICAgIGJ0blRleHQ9XCJMaXJlXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9IZWFkZXI+XG4gICAgICAgICAgPEF1eD5cbiAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnNlYXJjaH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMudXBkYXRlU2VhcmNofVxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIlJlY2hlcmNoZXIgdW4gYXJ0aWNsZVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNlYXJjaEJveFwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgICAgICAuc2VhcmNoQm94IHtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDU2cHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDAwMDtcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgICAgICAgICAgICAgYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjZjlmOWY5O1xuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAuc2VhcmNoQm94OjpwbGFjZWhvbGRlciB7XG4gICAgICAgICAgICAgICAgLyogQ2hyb21lLCBGaXJlZm94LCBPcGVyYSwgU2FmYXJpIDEwLjErICovXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMTsgLyogRmlyZWZveCAqL1xuICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgLnNlYXJjaEJveDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgICAgIC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4yKTtcbiAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgIC5zZWFyY2hCb3g6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7XG4gICAgICAgICAgICAgICAgLyogTWljcm9zb2Z0IEVkZ2UgKi9cbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjIpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC5zZWFyY2hCb3g6Zm9jdXMge1xuICAgICAgICAgICAgICAgIG91dGxpbmU6IDA7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGB9PC9zdHlsZT5cbiAgICAgICAgICA8L0F1eD5cbiAgICAgICAgICB7bGlzdFBvc3RzfVxuICAgICAgICAgIDxGb290ZXIgLz5cbiAgICAgICAgPC9BdXg+XG4gICAgICA8L0xheW91dD5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNvbXBvc2UoXG4gIHdpdGhEYXRhLFxuICBncmFwaHFsKGFsbFBvc3RzLCB7XG4gICAgcHJvcHMoeyBkYXRhOiB7IGxvYWRpbmcsIHBvc3RzLCBmZXRjaE1vcmUgfSB9KSB7XG4gICAgICAvKipcbiAgICAgICAqIFJldHVybiB0aGUgcHJvcHMgdG8gY29ubmVjdCB0byB0aGUgY29tcG9uZW50XG4gICAgICAgKi9cbiAgICAgIHJldHVybiB7XG4gICAgICAgIGRhdGE6IHtcbiAgICAgICAgICBsb2FkaW5nLFxuICAgICAgICAgIHBvc3RzLFxuICAgICAgICAgIGZldGNoTW9yZSxcbiAgICAgICAgICBsb2FkTW9yZUVudHJpZXM6ICgpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBmZXRjaE1vcmUoe1xuICAgICAgICAgICAgICBxdWVyeTogYWxsUG9zdHMsXG4gICAgICAgICAgICAgIHZhcmlhYmxlczoge1xuICAgICAgICAgICAgICAgIGN1cnNvcjogcG9zdHMucGFnZUluZm8uZW5kQ3Vyc29yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHVwZGF0ZVF1ZXJ5OiAobmV4dFJlc3VsdCwgeyBmZXRjaE1vcmVSZXN1bHQgfSkgPT4ge1xuICAgICAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICAgICAqIFBsdWNrIHRoZSBuZXcgZWRnZXMgb3V0IG9mIHRoZSBxdWVyeSByZXN1bHRzXG4gICAgICAgICAgICAgICAgICovXG4gICAgICAgICAgICAgICAgY29uc3QgbmV3RWRnZXMgPSBmZXRjaE1vcmVSZXN1bHQucG9zdHMuZWRnZXM7XG5cbiAgICAgICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAgICAgKiBQbHVjayB0aGUgbmV3IHBhZ2VJbmZvIG91dCBvZiB0aGUgcXVlcnkgcmVzdWx0c1xuICAgICAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgICAgIGNvbnN0IHBhZ2VJbmZvID0gZmV0Y2hNb3JlUmVzdWx0LnBvc3RzLnBhZ2VJbmZvO1xuXG4gICAgICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgICAgICogUmV0dXJuIHRoZSBtb3ZpZXMgd2l0aCB0aGUgbmV3IGVkZ2VzIG1lcmdlZCB3aXRoIHRoZSBleGlzdGluZyBvbmVzLCBhbmRcbiAgICAgICAgICAgICAgICAgKiB0aGUgbmV3IHBhZ2VJbmZvIHJlcGxhY2luZyB0aGUgb2xkIHBhZ2VJbmZvXG4gICAgICAgICAgICAgICAgICovXG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgIHBvc3RzOiB7XG4gICAgICAgICAgICAgICAgICAgIGVkZ2VzOiBbLi4ubmV4dFJlc3VsdC5wb3N0cy5lZGdlcywgLi4ubmV3RWRnZXNdLFxuICAgICAgICAgICAgICAgICAgICBwYWdlSW5mb1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9XG4gIH0pXG4pKEluZGV4KTtcbiJdfQ== */\n/*@ sourceURL=/Users/aure/Documents/Projets/Eugenie/pages/index.js */")), listPosts, __jsx(_components_Footer_Footer__WEBPACK_IMPORTED_MODULE_21__["default"], null)));
    }
  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_apollo__WEBPACK_IMPORTED_MODULE_9__["compose"])(_lib_withData__WEBPACK_IMPORTED_MODULE_11__["default"], Object(react_apollo__WEBPACK_IMPORTED_MODULE_9__["graphql"])(_lib_queries_posts__WEBPACK_IMPORTED_MODULE_10__["allPosts"], {
  props: function props(_ref) {
    var _ref$data = _ref.data,
        loading = _ref$data.loading,
        posts = _ref$data.posts,
        fetchMore = _ref$data.fetchMore;

    /**
     * Return the props to connect to the component
     */
    return {
      data: {
        loading: loading,
        posts: posts,
        fetchMore: fetchMore,
        loadMoreEntries: function loadMoreEntries() {
          return fetchMore({
            query: _lib_queries_posts__WEBPACK_IMPORTED_MODULE_10__["allPosts"],
            variables: {
              cursor: posts.pageInfo.endCursor
            },
            updateQuery: function updateQuery(nextResult, _ref2) {
              var fetchMoreResult = _ref2.fetchMoreResult;

              /**
               * Pluck the new edges out of the query results
               */
              var newEdges = fetchMoreResult.posts.edges;
              /**
               * Pluck the new pageInfo out of the query results
               */

              var pageInfo = fetchMoreResult.posts.pageInfo;
              /**
               * Return the movies with the new edges merged with the existing ones, and
               * the new pageInfo replacing the old pageInfo
               */

              return {
                posts: {
                  edges: [].concat(Object(_babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(nextResult.posts.edges), Object(_babel_runtime_corejs2_helpers_esm_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__["default"])(newEdges)),
                  pageInfo: pageInfo
                }
              };
            }
          });
        }
      }
    };
  }
}))(Index));

/***/ })

})
//# sourceMappingURL=index.js.00170c0345ff2d9c336d.hot-update.js.map