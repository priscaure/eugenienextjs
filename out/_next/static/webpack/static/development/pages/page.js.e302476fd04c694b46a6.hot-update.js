webpackHotUpdate("static/development/pages/page.js",{

/***/ "./pages/page.js":
/*!***********************!*\
  !*** ./pages/page.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/promise */ "./node_modules/@babel/runtime-corejs2/core-js/promise.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-apollo */ "./node_modules/react-apollo/react-apollo.browser.umd.js");
/* harmony import */ var react_apollo__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_apollo__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_reveal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-reveal */ "./node_modules/react-reveal/index.js");
/* harmony import */ var react_reveal__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_reveal__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_HOC_Aux__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../components/HOC/Aux */ "./components/HOC/Aux.js");
/* harmony import */ var _components_HOC_Layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/HOC/Layout */ "./components/HOC/Layout.js");
/* harmony import */ var _components_Header_Header__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../components/Header/Header */ "./components/Header/Header.js");
/* harmony import */ var _components_Layouts_Section_Section__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../components/Layouts/Section/Section */ "./components/Layouts/Section/Section.js");
/* harmony import */ var _components_Footer_Footer__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../components/Footer/Footer */ "./components/Footer/Footer.js");
/* harmony import */ var _lib_queries_page__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../lib/queries/page */ "./lib/queries/page.js");
/* harmony import */ var _lib_withData__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../lib/withData */ "./lib/withData.js");
/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../styles/style.css */ "./styles/style.css");
/* harmony import */ var _styles_style_css__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_styles_style_css__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_19__);









var __jsx = react__WEBPACK_IMPORTED_MODULE_7___default.a.createElement;














var PageTemplate =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_6__["default"])(PageTemplate, _Component);

  function PageTemplate() {
    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_2__["default"])(this, PageTemplate);

    return Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(PageTemplate).apply(this, arguments));
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_3__["default"])(PageTemplate, [{
    key: "render",
    value: function render() {
      var data = this.props.data;
      var loading = data.loading;

      if (loading) {
        console.log(data);
        return __jsx("div", null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_19___default.a, null, __jsx("link", {
          rel: "stylesheet",
          type: "text/css",
          href: "/nprogress.css"
        })));
      }

      var page = this.props.data.pageBy;
      return __jsx(_components_HOC_Layout__WEBPACK_IMPORTED_MODULE_11__["default"], null, __jsx(_components_HOC_Aux__WEBPACK_IMPORTED_MODULE_10__["default"], null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_19___default.a, null, __jsx("title", null, page.title)), __jsx("style", {
        dangerouslySetInnerHTML: {
          __html: _styles_style_css__WEBPACK_IMPORTED_MODULE_17___default.a
        }
      }), __jsx(_components_Header_Header__WEBPACK_IMPORTED_MODULE_12__["default"], {
        key: page.id,
        title: page.title
      }), __jsx(_components_Layouts_Section_Section__WEBPACK_IMPORTED_MODULE_13__["default"], {
        classe: "singlePost"
      }, __jsx("div", {
        className: "sectionContentWrapper"
      }, __jsx(react_reveal__WEBPACK_IMPORTED_MODULE_9__["Fade"], {
        delay: 500
      }, __jsx("div", {
        dangerouslySetInnerHTML: {
          __html: page.content
        }
      })))), __jsx(_components_Footer_Footer__WEBPACK_IMPORTED_MODULE_14__["default"], null)));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var uri;
      return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.async(function getInitialProps$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              uri = _ref.query.uri;
              _context.next = 3;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.awrap(new _babel_runtime_corejs2_core_js_promise__WEBPACK_IMPORTED_MODULE_1___default.a(function (resolve) {
                setTimeout(resolve, 600);
              }));

            case 3:
              console.log("uri" + uri);
              return _context.abrupt("return", {
                uri: uri
              });

            case 5:
            case "end":
              return _context.stop();
          }
        }
      });
    }
  }]);

  return PageTemplate;
}(react__WEBPACK_IMPORTED_MODULE_7__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(react_apollo__WEBPACK_IMPORTED_MODULE_8__["compose"])(_lib_withData__WEBPACK_IMPORTED_MODULE_16__["default"], Object(react_apollo__WEBPACK_IMPORTED_MODULE_8__["graphql"])(_lib_queries_page__WEBPACK_IMPORTED_MODULE_15__["singlePage"], {
  options: function options(_ref2) {
    var uri = _ref2.uri;
    return {
      variables: {
        uri: uri
      }
    };
  }
}))(PageTemplate));

/***/ })

})
//# sourceMappingURL=page.js.e302476fd04c694b46a6.hot-update.js.map