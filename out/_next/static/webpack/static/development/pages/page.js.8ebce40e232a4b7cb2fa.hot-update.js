webpackHotUpdate("static/development/pages/page.js",{

/***/ "./lib/queries/page.js":
/*!*****************************!*\
  !*** ./lib/queries/page.js ***!
  \*****************************/
/*! exports provided: singlePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "singlePage", function() { return singlePage; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_1__);




function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n  query singlePage($uri: String) {\n    pageBy(uri: $uri) {\n      \n        slug\n        id\n        title\n        content\n    }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}


var singlePage = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(_templateObject());

/***/ })

})
//# sourceMappingURL=page.js.8ebce40e232a4b7cb2fa.hot-update.js.map