const express = require("express");
const compression = require("compression");
const next = require("next");
const path = require("path");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev, preserveLog: true });
const handle = app.getRequestHandler();

const options = {
  root: path.join(__dirname, "/public"),
  headers: {
    "Content-Type": "text/plain;charset=UTF-8"
  }
};

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(compression());

    server.get("/google269727bd96bc966d.html", (req, res) =>
      res.status(200).sendFile("google269727bd96bc966d.html", options)
    );

    server.get("/robots.txt", (req, res) =>
      res.status(200).sendFile("robots.txt", options)
    );

    server.get("/sitemap.xml", (req, res) =>
      res.status(200).sendFile("sitemap.xml", options)
    );

    server.get("/post/:slug", (req, res) => {
      const actualPage = "/post";
      const queryParams = { slug: req.params.slug };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/categorie/:slug", (req, res) => {
      const actualPage = "/categorie";
      const queryParams = { slug: req.params.slug };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("/page/:uri", (req, res) => {
      const actualPage = "/page";
      const queryParams = { uri: req.params.uri };
      app.render(req, res, actualPage, queryParams);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(3000, err => {
      if (err) throw err;
      console.log("> Ready on http://localhost:3000");
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
